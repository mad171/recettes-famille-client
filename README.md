# RecettesFamilleClient

RecettesFamille is a family project of recipe website allowing you to publish your recipes and share them with other users.
You can browse the recipes with filters to find recipes from a chief you admire, or with specific ingredients. You can also post comments on recipes you loved !

Bonus for developer performance :
- The Admin status exists and can be granted by Ludo, or another admin
- I added a feedback page in the app, so any user can give a feedback (general purpose, design focused, bug reporting, ...)

![Recettes-Famille Preview](./doc_images/recettes-famille2.gif)


# Go see it yourself !
The website is available at [this url](https://recettes-famille.herokuapp.com) ! (I chose to host it on a free service of Heroku, so the first attempt to reach the website might take 10/15sec).

# Run it and develop on your own

1) Make sure you have [Node and npm](https://nodejs.org/en/download/) installed
2) Then you just have to install all the required modules. Since it includes Angular, it might take 5 min.
3) Then just run the `npm start` command


# Development details
*This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.*
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
