console.log('Lauching server');
const express = require('express');
const app = express();
const path = require('path');

app.use(express.static(__dirname + '/dist/recettes-famille-client'));

app.listen(process.env.PORT || 3000);

// PathLocationStrategy

app.get('/*', function (req, res) {
    console.log('trying to sendFile from :' + __dirname + '/dist/recettes-famille-client/index.html');
    res.sendFile(path.join(__dirname + '/dist/recettes-famille-client/index.html'));
});

console.log('Console listening !');