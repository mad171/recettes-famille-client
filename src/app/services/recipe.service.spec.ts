import { TestBed, inject } from '@angular/core/testing';
import { RecipeService } from './recipe.service';
import { HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';


describe('RecipeService', () => {

  // ---- SERVICES ----
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule, RouterTestingModule],
      providers: [{ provide: HttpClient, useValue: httpClientSpy }]
    });
  });

  it('should be created', inject([RecipeService], (service: RecipeService) => {
    expect(service).toBeTruthy();
  }));
});
