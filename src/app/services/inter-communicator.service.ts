import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Recipe } from '../models/recipe';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})

/** Intercommunication service which helps detect changes between components of the app */
export class InterCommunicatorService {

  constructor(private messageService: MessageService) { }

  // Observable to detect search terms changing
  public searchValueHasChanged = new Subject<string>();
  observatorSearchValue = this.searchValueHasChanged.asObservable();

  // Observable to detect the need to refresh nav panel
  private navPanelHasToRefresh = new BehaviorSubject('default message');
  observatorNavPanelRefresh = this.navPanelHasToRefresh.asObservable();

  // Observable to detect the need to refresh chat panel
  private chatPanelHasToRefresh = new Subject<Recipe>();
  observatorChatPanelRefresh = this.chatPanelHasToRefresh.asObservable();

  // Observable to detect a change in login state
  private loginHasChanged = new Subject<boolean>();
  observatorLoginHasChanged = this.loginHasChanged.asObservable();


  /** Edits the Subject navPanelHasToRefresh to wich RecipeAddComponent and RecipeAddComponent have subscribed so that
   * the list of recipes is refreshed in the nav-panel-component.
   */
  refreshNavPanel(message: string) {
    this.navPanelHasToRefresh.next(message);
    this.log('a recipe has been added, NavPanel has to refresh !');
  }

  /** Edits the Subject searchValueHasChanged to wich RecipeSearchResult has subscribed so that
   * the list of recipes is refreshed in the recipe-search-result-component.
   */
  searchValueChanged(newSearchTerms: string) {
    this.searchValueHasChanged.next(newSearchTerms);
    this.log('search value has changed !');
  }

  /** Edits the Subject chatPanelHasToRefresh to wich navPanel has subscribed so that
 * the chat chanel is refreshed in the chat-component.
 */
  recipeChanged(newRecipe: Recipe) {
    this.chatPanelHasToRefresh.next(newRecipe);
    this.log('recipe has changed --> id:' + newRecipe.recip_id);
  }

  /** Edits the Subject loginHasChanged to wich registerComponent and loginComponent have subscribed so that
 * login state is refreshed in the app-component.
 */
  loginChanged(newLoginState: boolean) {
    this.loginHasChanged.next(newLoginState);
    this.log('new user login state, loggedIn = ' + newLoginState);
  }

  /** Log a RecipeService message with the MessageService */
  private log(message: string) {
    this.messageService.add('InterCommService: ' + message);
  }


}
