import { TestBed, inject } from '@angular/core/testing';

import { InterCommunicatorService } from './inter-communicator.service';

describe('InterCommunicatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterCommunicatorService]
    });
  });

  it('should be created', inject([InterCommunicatorService], (service: InterCommunicatorService) => {
    expect(service).toBeTruthy();
  }));
});
