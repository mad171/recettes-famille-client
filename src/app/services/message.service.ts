import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/** A message Service to add message to the user console
 * which displays messages about the web app running
 */
export class MessageService {

  messages: string[] = [];

  constructor() { }

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}

