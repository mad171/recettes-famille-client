import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe';

import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { AuthentificationService } from './authentification.service';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

/** Recipe service sends request to the server about recipes */
export class RecipeService {

    private recipesUrl = environment.back_url + '/recipe';
    private currentRecipeId = -1;
    private returnedObservable: Observable<any>;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Token ' + this.authService.getToken()
        })
    };

    constructor(
        private http: HttpClient,
        private messageService: MessageService,
        private authService: AuthentificationService) { }


    // ________________________________________________________ \\
    // _________________________ GETS _________________________ \\

    /** GET all the recipes with a request url /all */
    getRecipes(): Observable<Recipe[]> {
        const url = `${this.recipesUrl}/all`;
        this.returnedObservable = this.http.get(url, this.httpOptions)
            .pipe(
                tap(recipes => this.log(`fetched recipes`)),
                catchError(this.handleError('getRecipes', []))
            );
        return this.returnedObservable;
    }

    /** GET a single recipe by its id. Will 404 if id not found (see handleError method) */
    getRecipe(id: number): Observable<Recipe> {
        const url = `${this.recipesUrl}/id/${id}`;
        this.returnedObservable = this.http.get(url, this.httpOptions)
            .pipe(
                tap(_ => this.log(`fetched recipe id=${id}`)),
                catchError(this.handleError<Recipe>(`getRecipe id=${id}`))
            );
        return this.returnedObservable;
    }

    /** SEARCH ALL RECIPES MATCHING THE SEARCH CRITERIA */
    searchRecipes(search: Object): Observable<Recipe[]> {
        const url = `${this.recipesUrl}/search/`;
        this.returnedObservable = this.http.post(`${this.recipesUrl}/search/`, { search: search }, this.httpOptions)
            .pipe(
                tap(_ => this.log(`found recipes matching ${search}`)),
                catchError(this.handleError<Recipe[]>('searchRecipe', []))
            );
        return this.returnedObservable;
    }


    // ________________________________________________________ \\
    // _________________________ PUTS _________________________ \\

    /** PUT: sends an update request to Back end for a recipe */
    updateRecipe(recipe: Recipe, only_comments: boolean): Observable<any> {
        // TODO
        const url = `${this.recipesUrl}/id/${recipe.recip_id}/${only_comments}`;
        this.returnedObservable = this.http.put(url, recipe, this.httpOptions)
            .pipe(
                tap(_ => this.log(`updated recipe id=${recipe.recip_id}`)),
                catchError(this.handleError<any>(`updateRecipe`))
            );
        return this.returnedObservable;
    }


    // ________________________________________________________ \\
    // _________________________ POSTS ________________________ \\

    /** POST: sends an add request to Back end for a new recipe */
    saveRecipe(recipe: Recipe): Observable<Recipe> {
        const url = `${this.recipesUrl}/add`;
        this.returnedObservable = this.http.post<Recipe>(url, recipe, this.httpOptions)
            .pipe(
                tap(_ => this.log(`added recipe`)),
                catchError(this.handleError<Recipe>('addRecipe'))
            );
        return this.returnedObservable;
    }


    // ________________________________________________________ \\
    // ________________________ DELETE ________________________ \\

    /** DELETE: deletes the recipe from the server */
    deleteRecipe(recipe: Recipe | number): Observable<Recipe> {
        const id = typeof recipe === 'number' ? recipe : recipe.recip_id;
        const url = `${this.recipesUrl}/id/${id}`;

        this.returnedObservable = this.http.delete<Recipe>(url, this.httpOptions).pipe(
            tap(_ => this.log(`deleted recipe id=${id}`)),
            catchError(this.handleError<Recipe>('deleteRecipe'))
        );
        return this.returnedObservable;
    }


    setCurrentRecipeId(id: number) {
        this.currentRecipeId = id;
    }
    getCurrentRecipeId() {
        return this.currentRecipeId;
    }


    /** Log a RecipeService message with the MessageService */
    private log(message: string) {
        this.messageService.add('RecipeService: ' + message);
    }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
