import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { AuthentificationService } from './authentification.service';
import { environment } from '../../environments/environment';
import { Observable, of } from '../../../node_modules/rxjs';
import { HttpHeaders, HttpClient } from '../../../node_modules/@angular/common/http';
import { Feedback } from '../models/feedback';
import { tap, catchError } from '../../../node_modules/rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  private feedbacksUrl = environment.back_url + '/feedback';
  private returnedObservable: Observable<any>;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Token ' + this.authService.getToken()
    })
  };
  constructor(private http: HttpClient,
    private messageService: MessageService,
    private authService: AuthentificationService) { }

  // ________________________________________________________ \\
  // _________________________ GETS _________________________ \\

  /** GET all the feedbacks from a category with a request url /all */
  getFeedbacksByCategory(category: string): Observable<Feedback[]> {
    const url = `${this.feedbacksUrl}/all/${category}`;
    this.returnedObservable = this.http.get(url, this.httpOptions)
      .pipe(
        tap(recipes => this.log(`fetched recipes`)),
        catchError(this.handleError(`getFeebacks category=${category}`))
      );
    return this.returnedObservable;
  }

  // ________________________________________________________ \\
  // _________________________ POSTS ________________________ \\

  /** POST: sends an add request to Back end for a new recipe */
  saveFeedback(feedback: Feedback): Observable<Feedback> {
    const url = `${this.feedbacksUrl}/add`;
    this.returnedObservable = this.http.post<Feedback>(url, feedback, this.httpOptions)
      .pipe(
        tap(_ => this.log(`added feedback`)),
        catchError(this.handleError<Feedback>('saveFeedback'))
      );
    return this.returnedObservable;
  }


  // ________________________________________________________ \\
  // ________________________ DELETE ________________________ \\

  /** DELETE: deletes the recipe from the server */
  deleteFeedback(feedback: Feedback | number): Observable<Feedback> {
    const id = typeof feedback === 'number' ? feedback : feedback.feedback_id;
    const url = `${this.feedbacksUrl}/delete/${id}`;

    this.returnedObservable = this.http.delete<Feedback>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted feedback id=${id}`)),
      catchError(this.handleError<Feedback>('deleteFeedback'))
    );
    return this.returnedObservable;
  }


  /** Log a RecipeService message with the MessageService */
  private log(message: string) {
    this.messageService.add('RecipeService: ' + message);
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
