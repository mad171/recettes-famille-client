import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { TokenPayLoad } from '../models/token-pay-load';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

/** Authentification service which enables to register login or get the profile of a user
   * and checks weither the user's token is expired
   */
export class AuthentificationService {
  private token: string;
  private userUrl = environment.back_url + '/user';
  constructor(private http: HttpClient, private router: Router) { }


  public register(tokenPayLoad: TokenPayLoad): Observable<any> {
    return this.request('post', 'register', tokenPayLoad);
  }

  public login(tokenPayLoad: TokenPayLoad): Observable<any> {
    return this.request('post', 'login', tokenPayLoad);
  }

  public profile(): Observable<any> {
    return this.request('get', 'profile');
  }

  /** General requests for user management */
  private request(method: 'post' | 'get', type: 'login' | 'register' | 'profile', user?: TokenPayLoad): Observable<any> {
    let base;

    if (method === 'post') {
      const req = { user: user };
      base = this.http.post(`${this.userUrl}/${type}`, req);
    } else {
      base = this.http.get(`${this.userUrl}/${type}`, { headers: { Authorization: `Token ${this.getToken()}` } });
    }

    const request = base.pipe(
      map((res: { user: { _id: string, nickname: string, email: string, token: string } }) => {
        const data = res.user;
        if (data.token) {
          this.saveToken(data.token);
        }
        return data;
      })
    );

    return request;
  }

  /** Checks if the user is logged in
   * ie : if he has a token checks if it is expired or not
   */
  public isLoggedIn(): boolean {
    const user = this.getUserDetails();
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  /** Get user details from its token */
  public getUserDetails(): User {
    const token = this.getToken();
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  /** Saves the user's token in local storage */
  private saveToken(token: string): void {
    localStorage.setItem('MyToken', token);
    this.token = token;
  }

  /** Get the token of the user */
  public getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('MyToken');
    }
    return this.token;
  }

  /** Logout and remove Token from local storage */
  public logout(): void {
    this.token = '';
    window.localStorage.removeItem('MyToken');
    this.router.navigateByUrl('signpage');
  }

}
