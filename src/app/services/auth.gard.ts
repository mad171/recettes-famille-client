import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthentificationService } from './authentification.service';

@Injectable()

/** Authentification Guard for the front's routes */
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthentificationService) { }

    canActivate() {
        if (this.authService.isLoggedIn()) {
            return true;
        }
        // redirect to home page '/signpage'
        alert('Connectez vous à votre compte de Chef pour y accéder !');
        this.router.navigate(['signpage']);
        return false;
    }

}
