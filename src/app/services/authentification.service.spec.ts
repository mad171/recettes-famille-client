import { TestBed, inject } from '@angular/core/testing';

import { AuthentificationService } from './authentification.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

describe('AuthentificationService', () => {

  // ---- SERVICES ----
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: Router,
        useValue: routerSpy,
      },
      {
        provide: HttpClient,
        useValue: httpClientSpy,
      },
      ]
    });
  });

  it('should be created', inject([AuthentificationService], (service: AuthentificationService) => {
    expect(service).toBeTruthy();
  }));
});
