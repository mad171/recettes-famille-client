import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './services/auth.gard';
import { SignPageComponent } from './user/sign-page/sign-page.component';
import { HostPageComponent } from './search/host-page/host-page.component';
import { RecipeViewerComponent } from './recipe/recipe-viewer/recipe-viewer.component';
import { RecipeCreatorComponent } from './recipe/recipe-creator/recipe-creator.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { SearchComponent } from './search/search/search.component';
import { FeedbackComponent } from './user/feedback/feedback.component';

@NgModule({
  declarations: [
    AppComponent,
    SignPageComponent,
    HostPageComponent,
    RecipeViewerComponent,
    RecipeCreatorComponent,
    LoginComponent,
    RegisterComponent,
    SearchComponent,
    FeedbackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
