import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignPageComponent } from './user/sign-page/sign-page.component';
import { AuthGuard } from './services/auth.gard';
import { HostPageComponent } from './search/host-page/host-page.component';
import { RecipeViewerComponent } from './recipe/recipe-viewer/recipe-viewer.component';
import { RecipeCreatorComponent } from './recipe/recipe-creator/recipe-creator.component';
import { FeedbackComponent } from './user/feedback/feedback.component';

const routes: Routes = [
  { path: 'signpage', component: SignPageComponent },
  { path: 'hostpage', component: HostPageComponent, canActivate: [AuthGuard] },
  { path: 'recipe', component: RecipeViewerComponent, canActivate: [AuthGuard] },
  { path: 'newrecipe', component: RecipeCreatorComponent, canActivate: [AuthGuard] },
  { path: 'feedback', component: FeedbackComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'signpage' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
