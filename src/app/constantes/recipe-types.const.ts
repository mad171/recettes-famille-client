/** List of types which can be associated to a recipe */
export const recipeTypes = {
    ENTREE: 'Entrée',
    PLAT: 'Plat',
    DESSERT: 'Dessert',
    BOISSON: 'Boisson',
    AUTRE: 'Autre'
};
