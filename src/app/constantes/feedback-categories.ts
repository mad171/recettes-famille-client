/** List of categories which can be associated to a feedback */
export const feedbackCategories = {
    GENERAL: 'Général',
    BUGS: 'Bugs',
    DESIGN: 'Design',
    FONCTIONNALITES: 'Fonctionnalités'
};
