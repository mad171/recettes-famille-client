/** List of levels of difficulty which can be associated to a recipe */
export const recipeLevels = {
    EASY: '★',
    MEDIUM: '★★',
    HARD: '★★★',
    HARDCORE: '★★★★'
};
