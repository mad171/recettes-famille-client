import { Component } from '@angular/core';
import { TokenPayLoad } from '../../models/token-pay-load';
import { AuthentificationService } from '../../services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../../style/css/register.component.css']
})
export class RegisterComponent {

  credentials: TokenPayLoad = {
    nickname: '',
    password: ''
  };
  confirmPassword: '';

  constructor(private auth: AuthentificationService, private router: Router) { }

  /** Sends a request for a new registration provided that the password has been well confirmed */
  register() {
    this.displayLoader('registerLoader', 'Bienvenue chef !', 'Il y a plein de recettes à découvrir et à partager, au boulot !');
    const chkNickname = this.checkNickname();
    const chkPwd = this.checkPasswordsValidation();

    if (chkNickname !== 'Valide' || chkPwd !== 'Valide') {
      alert('nickname : ' + chkNickname + '\npassword : ' + chkPwd);
      this.hideLoader('registerLoader');
    }
    else {
      this.auth.register(this.credentials).subscribe(() => {
        this.router.navigateByUrl('/hostpage');
      }, (err) => {
        this.hideLoader('registerLoader');
        const errorsBody = err.error.errors;
        alert('Database Error : \n \tnickname : ' + errorsBody.nickname);
      });
    }
  }

  /** When user types 'enter', focus switch to the next input */
  focusNextInput(id: string): void {
    document.getElementById(id).focus();
  }

  /** Validates the nickname
   * @requires 'nickname'
   */
  checkNickname(): string {
    let validationNickname = '';
    if (!this.credentials.nickname) {
      validationNickname = 'required';
    }
    else if (this.credentials.nickname.length < 6) {
      validationNickname = 'length should be greater than 6. Set it as "FirstName Lastname" so that your mate recognize you';
    }
    else {
      validationNickname = 'Valide';
    }
    return validationNickname;
  }

  /** Validates the password
    * @requires password.length > 6
    * @requires password === confirmPassword
    */
  checkPasswordsValidation(): string {
    let validationMdp = '';
    if (this.credentials.password.length < 6) {
      validationMdp = 'Mot de passe doit contenir plus de 6 caractères';
    } else if (this.credentials.password !== this.confirmPassword) {
      validationMdp = 'Mauvaise confirmation';
    } else {
      validationMdp = 'Valide';
    }
    return validationMdp;
  }


  displayLoader(loaderId: string, title: string, content: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'flex';
    const messageTitle = <HTMLElement>loader.firstChild.firstChild;
    messageTitle.innerHTML = title;
    const messageContent = <HTMLElement>loader.firstChild.lastChild;
    messageContent.innerHTML = content;
  }

  hideLoader(loaderId: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'none';
  }
}
