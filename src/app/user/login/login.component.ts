import { Component } from '@angular/core';
import { TokenPayLoad } from '../../models/token-pay-load';
import { AuthentificationService } from '../../services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../style/css/login.component.css']
})

/** Login component which endorses the login form */
export class LoginComponent {

  credentials: TokenPayLoad = {
    nickname: '',
    password: ''
  };

  constructor(private auth: AuthentificationService, private router: Router) { }

  /** Sends a login request through the auth Service.
   * If logins are correct, it changes the loggedState so that content is enabled by AppComponent
   */
  login(): void {
    this.displayLoader('loginLoader', 'Bonjour chef !', 'Nous préparons vos outils !');
    this.auth.login(this.credentials).subscribe(
      () => { this.router.navigateByUrl('hostpage'); },
      (err) => {
        this.hideLoader('loginLoader');
        const errorsBody = err.error.errors;
        alert('Database Error : \nnickname : ' + errorsBody.nickname + '\npassword : ' + errorsBody.password);
      });
  }

  /** When user types 'enter', focus switch to the next input */
  focusNextInput(id: string): void {
    document.getElementById(id).focus();
  }


  displayLoader(loaderId: string, title: string, content: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'flex';
    const messageTitle = <HTMLElement>loader.firstChild.firstChild;
    messageTitle.innerHTML = title;
    const messageContent = <HTMLElement>loader.firstChild.lastChild;
    messageContent.innerHTML = content;
  }

  hideLoader(loaderId: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'none';
  }

}
