import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Feedback } from '../../models/feedback';
import { FeedbackService } from '../../services/feedback.service';
import { AuthentificationService } from '../../services/authentification.service';
import { createTokenForExternalReference } from '../../../../node_modules/@angular/compiler/src/identifiers';
import { feedbackCategories } from '../../constantes/feedback-categories';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['../../style/css/feedback.component.css']
})
export class FeedbackComponent implements OnInit, AfterViewInit {

  currentCategory = 'Général';
  categories = [];
  feedbacksList: Feedback[];
  newFeedback = new Feedback();
  userNickname: string;
  userHasRights: Boolean = false;

  constructor(private feedbackService: FeedbackService, private auth: AuthentificationService) { }

  ngOnInit() {
    this.getFeedbacks(this.currentCategory);
    this.getFeedbackCategories();
  }

  ngAfterViewInit() {
    if (this.userHasRights) {
      this.setCustomSelect('select-feedback-category');
    }
  }


  /** Checks wheither the user has the right to delete feedbacks */
  getUserRights(): void {
    this.auth.profile().subscribe(user => {
      this.userHasRights = user.isAdmin;
      this.userNickname = user.nickname;
      this.setCustomSelect('select-feedback-category');
    });
  }

  getFeedbacks(category: string): void {
    this.displayLoader('feedbackLoader', 'Chef,', 'Nous cherchons les suggestions !');
    this.feedbackService.getFeedbacksByCategory(category)
      .subscribe(feedbacks => {
        this.feedbacksList = feedbacks;
        this.feedbacksList.sort(function compare(fA, fB) {
          const dateA = new Date(fA.date).getTime();
          const dateB = new Date(fB.date).getTime();
          return dateB - dateA;
        });
        this.getUserRights();
        this.hideLoader('feedbackLoader');
      });
  }

  /** Add all the allowed categories to the category selector with dom
* @see feedbackCategories in 'constantes'
*/
  getFeedbackCategories(): void {
    Object.keys(feedbackCategories).forEach(key => {
      this.categories.push(feedbackCategories[key]);
    });
  }

  addFeedback(): void {
    if (this.newFeedback.title.length > 100) { alert('Votre titre est trop long chef :('); }
    else if (this.newFeedback.content !== '' && this.newFeedback.title !== '') {
      this.displayLoader('feedbackLoader', 'Merci pour votre message chef !', 'Nous l envoyons au développeur !');
      this.newFeedback.nickname = this.userNickname;
      this.newFeedback.category = this.currentCategory;
      this.newFeedback.date = new Date(Date.now());
      this.feedbackService.saveFeedback(this.newFeedback).subscribe(() => {
        this.feedbacksList.push(this.newFeedback);
        this.feedbacksList.sort(function compare(fA, fB) {
          const dateA = new Date(fA.date).getTime();
          const dateB = new Date(fB.date).getTime();
          return dateB - dateA;
        });
        this.newFeedback = new Feedback;
        this.hideLoader('feedbackLoader');
      });
    }
  }

  deleteFeedback(feedback: Feedback): void {
    this.feedbackService.deleteFeedback(feedback).subscribe(() => {
      this.feedbacksList.splice(this.feedbacksList.indexOf(feedback), 1);
    });
  }

  displayLoader(loaderId: string, title: string, content: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'flex';
    const messageTitle = <HTMLElement>loader.firstChild.firstChild;
    messageTitle.innerHTML = title;
    const messageContent = <HTMLElement>loader.firstChild.lastChild;
    messageContent.innerHTML = content;
  }

  hideLoader(loaderId: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'none';
  }


  setCustomSelect(select_id: string) {
    const select = document.getElementById(select_id);
    let selectLabelText;

    for (let i = 0; i < select.children.length; i++) {
      if (select.children[i].className === 'select-label') {
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'select-label-text') {
            selectLabelText = select.children[i].children[j];
          }
        }
      }
      if (select.children[i].className === 'options') {
        const dropdown = select.children[i];
        select.addEventListener('mouseleave', function () {
          dropdown.classList.remove('show');
        });
        select.addEventListener('mouseenter', function () {
          dropdown.classList.add('show');
        });
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'option-label') {
            select.children[i].children[j].addEventListener('click', function (e) {
              const event = e.target as HTMLLabelElement;
              selectLabelText.textContent = event.innerHTML;
              dropdown.classList.remove('show');
            });
          }
        }
      }

    }
  }

}
