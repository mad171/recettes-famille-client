import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { AuthentificationService } from '../../services/authentification.service';


@Component({
  selector: 'app-sign-page',
  templateUrl: './sign-page.component.html',
  styleUrls: ['../../style/css/sign-page.component.css']
})

export class SignPageComponent implements OnInit {

  constructor(private router: Router, private auth: AuthentificationService) { }

  ngOnInit() {
    if (this.auth.isLoggedIn()) { this.router.navigateByUrl('/hostpage'); }
  }

}
