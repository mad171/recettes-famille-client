export class User {
    _id: string;
    nickname: string;
    exp: number;
    iat: number;
}
