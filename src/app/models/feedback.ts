export class Feedback {
    _id: number;
    feedback_id: number;
    category: string;
    nickname: string;
    title: string;
    content: string;
    date: Date;
    _v: number;
}
