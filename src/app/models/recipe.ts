export class Recipe {
  _id: number;
  recip_name: string;
  recip_id: number;
  recip_author: string;
  recip_duration: number;
  recip_level: string;
  recip_type: string;
  recip_N_eaters: number;
  recip_ingredientList: { ingredient: string, quantity: string, unity: string }[];
  recip_explanations: string;
  recip_comments: { nickname: string, comment: string, date: Date }[];
  updated_at: Date;
  _v: number;
}
