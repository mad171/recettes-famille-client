import { Component } from '@angular/core';
import { AuthentificationService } from './services/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./style/css/app.component.css']
})
export class AppComponent {

  constructor(public auth: AuthentificationService) { }

  /** Logout Action
   * note that I have to reload page to avoid bugs in case the user log out and login whithout refreshing
   */
  logout(): void {
    this.auth.logout();
  }

  /** Toggle navigation dropdown content*/
  toggleNav() {
    document.getElementById('navDropdown').classList.toggle('show');
  }




}
