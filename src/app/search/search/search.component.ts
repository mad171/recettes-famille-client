import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';
import { Router } from '../../../../node_modules/@angular/router';
import { recipeTypes } from '../../constantes/recipe-types.const';
import { recipeLevels } from '../../constantes/recipe-levels.const';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['../../style/css/search.component.css']
})


export class SearchComponent implements OnInit, AfterViewInit {

  recipes: Recipe[];
  types = [];
  levels = [];

  search = {
    recip_name: [],
    recip_author: [],
    recip_duration: [],
    recip_level: [],
    recip_type: [],
    recip_N_eaters: [],
    recip_ingredientList: []
  };

  add_type = 'Tous';
  add_level = 'Tous';
  add_duration_min: number;
  add_duration_max: number;
  add_name: string;
  add_author: string;
  add_ingredient: string;


  constructor(private recipeService: RecipeService, private router: Router) { }

  ngOnInit() {
    this.getRecipeTypes();
    this.getRecipeLevels();
  }


  ngAfterViewInit() {
    this.setCustomSelect('select-type');
    this.setCustomSelect('select-level');
  }

  /** Send a request through recipeService to find the recipes matching the filters */
  searchRecipes() {
    this.displayLoader('searchLoader', 'Bien reçu chef !', 'Nous recherchons les recettes correspondantes !');
    if (this.add_level !== 'Tous') { this.search['recip_level'] = [this.add_level]; }
    else { this.search['recip_level'] = []; }
    if (this.add_type !== 'Tous') { this.search['recip_type'] = [this.add_type]; }
    else { this.search['recip_type'] = []; }
    if (this.add_duration_min !== undefined && this.add_duration_max !== undefined && this.add_duration_min < this.add_duration_max) {
      this.search['recip_duration'] = [this.add_duration_min, this.add_duration_max];
    }
    else { this.search['recip_duration'] = []; }
    this.recipes = [];
    this.recipeService.searchRecipes(this.search).subscribe(recipes => {
      this.recipes = recipes;
      this.hideLoader('searchLoader');
    });
  }


  newTag(key: string) {
    let tag = '';
    switch (key) {
      case 'recip_name':
        tag = this.add_name;
        this.add_name = '';
        break;
      case 'recip_author':
        tag = this.add_author;
        this.add_author = '';
        break;
      case 'recip_ingredientList':
        tag = this.add_ingredient;
        this.add_ingredient = '';
        break;
    }
    if (!this.search[key].includes(tag) && tag !== '' && tag !== undefined) { this.search[key].push(tag); }
  }

  deleteTag(key: string, tag: string) {
    const index = this.search[key].indexOf(tag);
    if (index > -1) {
      this.search[key].splice(index, 1);
    }
  }

  viewRecipe(recipeId: number) {
    this.recipeService.setCurrentRecipeId(recipeId);
    this.router.navigateByUrl('recipe');
  }


  /** Get all the types allowed to fill the selector
  * @see recipeTypes in 'constantes'
  */
  getRecipeTypes(): void {
    Object.keys(recipeTypes).forEach(key => {
      this.types.push(recipeTypes[key]);
    });
  }

  /** Get all the levels allowed to fill the selector
  * @see recipeLevels in 'constantes'
  */
  getRecipeLevels(): void {
    Object.keys(recipeLevels).forEach(key => {
      this.levels.push(recipeLevels[key]);
    });
  }




  displayLoader(loaderId: string, title: string, content: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'flex';
    const messageTitle = <HTMLElement>loader.firstChild.firstChild;
    messageTitle.innerHTML = title;
    const messageContent = <HTMLElement>loader.firstChild.lastChild;
    messageContent.innerHTML = content;
  }

  hideLoader(loaderId: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'none';
  }


  /** Set custom select behavior
   * @see mixin select in style/scss-utils/mixins
   */
  setCustomSelect(select_id: string) {
    const select = document.getElementById(select_id);
    let selectLabelText;

    for (let i = 0; i < select.children.length; i++) {
      if (select.children[i].className === 'select-label') {
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'select-label-text') {
            selectLabelText = select.children[i].children[j];
          }
        }
      }
      if (select.children[i].className === 'options') {
        const dropdown = select.children[i];
        select.addEventListener('mouseleave', function () {
          dropdown.classList.remove('show');
        });
        select.addEventListener('mouseenter', function () {
          dropdown.classList.add('show');
        });
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'option-label') {
            select.children[i].children[j].addEventListener('click', function (e) {
              const event = e.target as HTMLLabelElement;
              selectLabelText.textContent = event.innerHTML;
              dropdown.classList.remove('show');
            });
          }
        }
      }

    }
  }

}
