import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RecipeService } from '../../services/recipe.service';
import { Recipe } from '../../models/recipe';
import { Router } from '../../../../node_modules/@angular/router';
import { recipeTypes } from '../../constantes/recipe-types.const';
import { recipeLevels } from '../../constantes/recipe-levels.const';
import { AuthentificationService } from '../../services/authentification.service';

@Component({
  selector: 'app-recipe-viewer',
  templateUrl: './recipe-viewer.component.html',
  styleUrls: ['../../style/css/recipe-viewer.component.css']
})
export class RecipeViewerComponent implements OnInit, AfterViewInit {

  userHasRights: Boolean = false;
  recipe: Recipe = new Recipe;
  types = [];
  levels = [];

  ingredientList: { ingredient: string, quantity: string, unity: string }[];
  newIngredient = { ingredient: '', quantity: '', unity: '' };
  commentList: { nickname: string, comment: string, date: Date }[];
  newComment = { nickname: '', comment: '', date: new Date(Date.now()) };

  constructor(private recipeService: RecipeService, private auth: AuthentificationService, private router: Router) {
  }

  ngOnInit() {
    const recipeId = this.recipeService.getCurrentRecipeId();
    if (recipeId === -1) { this.router.navigateByUrl('hostpage'); }
    else {
      this.getRecipe(recipeId);
      this.getRecipeTypes();
      this.getRecipeLevels();
    }
  }

  ngAfterViewInit() {
    if (this.userHasRights) {
      this.setCustomSelect('select-recipe-type');
      this.setCustomSelect('select-recipe-level');
    }
  }

  /** Checks wheither the user has the right to edit recipe */
  getUserRights(): void {
    this.auth.profile().subscribe(user => {
      if (user.nickname === this.recipe.recip_author || user.isAdmin) {
        this.userHasRights = true;
        this.setCustomSelect('select-recipe-type');
        this.setCustomSelect('select-recipe-level');
      }
      else {
        this.makeInputsReadOnly();
      }
      this.newComment.nickname = user.nickname;
    });
  }

  getRecipe(id: number): void {
    this.displayLoader('recipeViewerLoader', 'Chef,', 'Nous cherchons la page de la recette !');
    this.recipeService.getRecipe(id)
      .subscribe(recipe => {
        this.recipe = recipe[0];
        this.ingredientList = this.recipe.recip_ingredientList;
        this.commentList = this.recipe.recip_comments;
        this.getUserRights();
        this.makeInputResizable(document.getElementById('nameRecipeInput'), null);
        this.hideLoader('recipeViewerLoader');
      });
  }


  /** Add all the allowed types to the type selector with dom
  * @see recipeTypes in 'constantes'
  */
  getRecipeTypes(): void {
    Object.keys(recipeTypes).forEach(key => {
      this.types.push(recipeTypes[key]);
    });
  }

  /** Add all the allowed types to the type selector with dom
  * @see recipeLevels in 'constantes'
  */
  getRecipeLevels(): void {
    Object.keys(recipeLevels).forEach(key => {
      this.levels.push(recipeLevels[key]);
    });
  }

  /** Add a new ingredient to the recipe's ingredient list table provided quantity is a number
   *  The new ingredient will only be saved at the recipe save
   */
  addIngredient(): void {
    let alreadyAdded = false;
    this.ingredientList.forEach(element => {
      if (element.ingredient === this.newIngredient.ingredient) {
        alreadyAdded = true;
      }
    });
    if (!alreadyAdded && this.newIngredient.ingredient !== '') {
      this.ingredientList.push(this.newIngredient);
      this.newIngredient = { ingredient: '', quantity: '', unity: '' };
      document.getElementById('inputIngredient').focus();
    }
    else {
      alert('L\'ingrédient n\'est pas valide ou Vous avez déjà ajouté cet ingrédient !');
    }
  }

  /** Delete an ingredient from the recipe's ingredient list table */
  deleteIngredient(ingr_name: string, ingr_quantity: string, ingr_unity: string): void {
    let i = 0;
    while (i < this.ingredientList.length) {
      if (this.ingredientList[i]['ingredient'] === ingr_name) { this.ingredientList.splice(i, 1); }
      i += 1;
    }
  }

  addComment(): void {
    if (this.newComment.comment !== '') {
      this.newComment.date = new Date(Date.now());
      this.commentList.push(this.newComment);
      this.commentList.sort(function compare(commentA, commentB) {
        const dateA = new Date(commentA.date).getTime();
        const dateB = new Date(commentB.date).getTime();
        return dateB - dateA;
      });
      this.newComment = { nickname: this.newComment.nickname, comment: '', date: new Date(Date.now()) };
      this.recipe.recip_comments = this.commentList;
      this.recipeService.updateRecipe(this.recipe, true).subscribe();
    }
  }

  deleteComment(comment: { nickname: string, comment: string }): void {
    let i = 0;
    while (i < this.commentList.length) {
      if (this.commentList[i]['nickname'] === comment.nickname && this.commentList[i]['comment'] === comment.comment) {
        this.commentList.splice(i, 1);
      }
      i += 1;
    }
    this.recipe.recip_comments = this.commentList;
    this.recipeService.updateRecipe(this.recipe, true).subscribe();
  }

  /** Save the changes made on the recipe */
  editRecipe(): void {
    if (isNaN(this.recipe.recip_N_eaters) || this.recipe.recip_N_eaters === null) {
      alert('Il faut que le nombre de personnes soit un nombre');
    }
    else {
      this.displayLoader('recipeViewerLoader', 'Bien compris chef !', 'Nous sauvegardons vos changements');
      this.recipe.recip_ingredientList = this.ingredientList;
      this.recipe.recip_comments = this.commentList;
      this.recipeService.updateRecipe(this.recipe, false).subscribe(() => {
        this.hideLoader('recipeViewerLoader');
        alert('Modifications enregistrées');
      });
    }
  }

  /** Delete the recipe from the database */
  deleteRecipe(): void {
    this.displayLoader('recipeViewerLoader', 'Vous avez raison chef !', 'Cette recette... c\' était du papier gaché');
    this.recipeService.deleteRecipe(this.recipe).subscribe(() => {
      this.hideLoader('recipeViewerLoader');
    });
    alert('Recette supprimée !');
  }



  displayLoader(loaderId: string, title: string, content: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'flex';
    const messageTitle = <HTMLElement>loader.firstChild.firstChild;
    messageTitle.innerHTML = title;
    const messageContent = <HTMLElement>loader.firstChild.lastChild;
    messageContent.innerHTML = content;
  }

  hideLoader(loaderId: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'none';
  }

  makeInputsReadOnly() {
    const dom_inputName = document.getElementById('nameRecipeInput');
    dom_inputName.setAttribute('readonly', '');
    const dom_selectType = document.getElementById('select-recipe-type');
    dom_selectType.setAttribute('disabled', '');
    const dom_selectLevel = document.getElementById('select-recipe-level');
    dom_selectLevel.setAttribute('readonly', '');
    const dom_selectDuration = document.getElementById('durationSelector');
    dom_selectDuration.setAttribute('readonly', '');
    const dom_inputNeaters = document.getElementById('recip_N_eatersSelector');
    dom_inputNeaters.setAttribute('readonly', '');
    const dom_explanations = document.getElementById('explanations-textarea');
    dom_explanations.setAttribute('readonly', '');
  }

  makeInputResizable(input, factor) {
    const int = Number(factor) || 0.50;
    function resize() {
      input.style.width = ((input.value.length + 1) * int) + 'em';
    }
    const e = 'keyup,keypress,focus,blur,change'.split(',');
    e.forEach(eventKeyWord => {
      input.addEventListener(eventKeyWord, function () {
        resize();
      });
    });
    if (this.recipe.recip_name !== undefined) { input.style.width = ((this.recipe.recip_name.length + 1) * int) + 'em'; }

  }

  setCustomSelect(select_id: string) {
    const select = document.getElementById(select_id);
    let selectLabelText;

    for (let i = 0; i < select.children.length; i++) {
      if (select.children[i].className === 'select-label') {
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'select-label-text') {
            selectLabelText = select.children[i].children[j];
          }
        }
      }
      if (select.children[i].className === 'options') {
        const dropdown = select.children[i];
        select.addEventListener('mouseleave', function () {
          dropdown.classList.remove('show');
        });
        select.addEventListener('mouseenter', function () {
          dropdown.classList.add('show');
        });
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'option-label') {
            select.children[i].children[j].addEventListener('click', function (e) {
              const event = e.target as HTMLLabelElement;
              selectLabelText.textContent = event.innerHTML;
              dropdown.classList.remove('show');
            });
          }
        }
      }

    }
  }


}
