import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';
import { Router } from '../../../../node_modules/@angular/router';
import { recipeTypes } from '../../constantes/recipe-types.const';
import { recipeLevels } from '../../constantes/recipe-levels.const';
import { AuthentificationService } from '../../services/authentification.service';

@Component({
  selector: 'app-recipe-creator',
  templateUrl: './recipe-creator.component.html',
  styleUrls: ['../../style/css/recipe-creator.component.css']
})
export class RecipeCreatorComponent implements OnInit, AfterViewInit {

  recipe: Recipe = new Recipe;
  ingredientList: { ingredient: string, quantity: string, unity: string }[];
  newIngredient = { ingredient: '', quantity: '', unity: '' };
  types = [];
  levels = [];

  constructor(private recipeService: RecipeService, private router: Router, private auth: AuthentificationService) {
  }

  ngOnInit() {
    this.auth.profile().subscribe(user => { this.recipe.recip_author = user.nickname; });
    this.ingredientList = [{ ingredient: 'default', quantity: 'default', unity: 'default' }];
    this.ingredientList.pop();
    this.getRecipeTypes();
    this.getRecipeLevels();
  }

  ngAfterViewInit() {
    this.makeInputResizable(document.getElementById('nameNewRecipeInput'), null);
    this.setCustomSelect('select-recipe-type');
    this.setCustomSelect('select-recipe-level');
  }


  /** Add all the allowed types to the type selector with dom
  * @see recipeTypes in 'constantes'
  */
  getRecipeTypes(): void {
    Object.keys(recipeTypes).forEach(key => {
      this.types.push(recipeTypes[key]);
    });
  }

  /** Add all the allowed types to the type selector with dom
  * @see recipeLevels in 'constantes'
  */
  getRecipeLevels(): void {
    Object.keys(recipeLevels).forEach(key => {
      this.levels.push(recipeLevels[key]);
    });
  }

  /** Add a new ingredient to the recipe's ingredient list table provided quantity is a number
   *  The new ingredient will only be saved at the recipe save
   */
  addIngredient(): void {
    let alreadyAdded = false;
    this.ingredientList.forEach(element => {
      if (element.ingredient === this.newIngredient.ingredient) {
        alreadyAdded = true;
      }
    });
    if (!alreadyAdded && this.newIngredient.ingredient !== '') {
      this.ingredientList.push(this.newIngredient);
      this.newIngredient = { ingredient: '', quantity: '', unity: '' };
      document.getElementById('inputIngredient').focus();
    }
    else {
      alert('L\'ingrédient n\'est pas valide ou Vous avez déjà ajouté cet ingrédient !');
    }
  }

  /** Delete an ingredient from the recipe's ingredient list table */
  deleteIngredient(ingr_name: string, ingr_quantity: string, ingr_unity: string): void {
    let i = 0;
    while (i < this.ingredientList.length) {
      if (this.ingredientList[i]['ingredient'] === ingr_name) { this.ingredientList.splice(i, 1); }
      i += 1;
    }
  }

  /** Save the new recipe */
  saveRecipe(): void {
    const missingElements = this.newRecipeMissingElements();
    if (this.newRecipeMissingElements().length === 0) {
      this.displayLoader('recipeCreatorLoader', 'Chef,', 'Nous publions votre chef d\'oeuvre !');
      this.recipe.recip_ingredientList = this.ingredientList;
      this.recipeService.saveRecipe(this.recipe).subscribe(() => {
        this.hideLoader('recipeCreatorLoader');
        alert('Recette publiée chef !');
        this.router.navigateByUrl('/hostpage');
      });
    }
    else {
      let errMsg = 'Chef, il manque les éléments suivants pour votre nouvelle recette :';
      missingElements.forEach(element => { errMsg += '\n - ' + element; });
      alert(errMsg);
    }
  }

  newRecipeMissingElements(): string[] {
    const missingElements = [];
    const recipeAttributes = Object.keys(this.recipe);
    console.log(this.recipe.recip_N_eaters);
    if (!recipeAttributes.includes('recip_name') || this.recipe.recip_name === '') { missingElements.push('Nom'); }
    if (!recipeAttributes.includes('recip_type') || this.recipe.recip_type === '') { missingElements.push('Type'); }
    if (!recipeAttributes.includes('recip_level') || this.recipe.recip_level === '') { missingElements.push('Niveau'); }
    if (!recipeAttributes.includes('recip_duration') || this.recipe.recip_duration === null) { missingElements.push('Durée'); }
    else if (isNaN(this.recipe.recip_duration) || this.recipe.recip_duration < 1) {
      missingElements.push('La durée de la recette doit être un nombre positif !');
    }
    if (this.ingredientList.length === 0) {
      missingElements.push('Ingrédients');
    }
    if (!recipeAttributes.includes('recip_N_eaters') || this.recipe.recip_N_eaters === null) {
      missingElements.push('Nombre de personnes');
    }
    else if (isNaN(this.recipe.recip_N_eaters) || this.recipe.recip_N_eaters < 1) {
      missingElements.push('Le nombre de personnes doit être un nombre positif !');
    }
    if (!recipeAttributes.includes('recip_explanations') || this.recipe.recip_explanations === '') { missingElements.push('Description'); }
    return missingElements;
  }


  displayLoader(loaderId: string, title: string, content: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'flex';
    const messageTitle = <HTMLElement>loader.firstChild.firstChild;
    messageTitle.innerHTML = title;
    const messageContent = <HTMLElement>loader.firstChild.lastChild;
    messageContent.innerHTML = content;
  }

  hideLoader(loaderId: string) {
    const loader = document.getElementById(loaderId);
    loader.style.display = 'none';
  }

  setCustomSelect(select_id: string) {
    const select = document.getElementById(select_id);
    let selectLabelText;

    for (let i = 0; i < select.children.length; i++) {
      if (select.children[i].className === 'select-label') {
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'select-label-text') {
            selectLabelText = select.children[i].children[j];
          }
        }
      }
      if (select.children[i].className === 'options') {
        const dropdown = select.children[i];
        select.addEventListener('mouseleave', function () {
          dropdown.classList.remove('show');
        });
        select.addEventListener('mouseenter', function () {
          dropdown.classList.add('show');
        });
        for (let j = 0; j < select.children[i].children.length; j++) {
          if (select.children[i].children[j].className === 'option-label') {
            select.children[i].children[j].addEventListener('click', function (e) {
              const event = e.target as HTMLLabelElement;
              selectLabelText.textContent = event.innerHTML;
              dropdown.classList.remove('show');
            });
          }
        }
      }

    }
  }

  makeInputResizable(input, factor) {
    const int = Number(factor) || 0.50;
    function resize() {
      input.style.width = ((input.value.length + 1) * int) + 'em';
    }
    const e = 'keyup,keypress,focus,blur,change'.split(',');
    e.forEach(eventKeyWord => {
      input.addEventListener(eventKeyWord, function () {
        resize();
      });
    });
  }



}
